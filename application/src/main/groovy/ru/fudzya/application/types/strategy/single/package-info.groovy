@GenericGenerators([
	@GenericGenerator(
		name       = 'SINGLE_PAYMENT_INSTRUMENT',
		strategy   = 'enhanced-sequence',
		parameters = [
				@Parameter(name = 'sequence_name', value = 'S_SINGLE_PAYMENT_INSTRUMENT')
		])
])

package ru.fudzya.application.types.strategy.single

import org.hibernate.annotations.GenericGenerator
import org.hibernate.annotations.GenericGenerators
import org.hibernate.annotations.Parameter