package ru.fudzya.application.types.strategy.union

import javax.persistence.*
import javax.validation.constraints.NotNull

@Entity
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
abstract class UnionPaymentInstrument {

    @Id
    @GeneratedValue(generator = 'UNION_PAYMENT_INSTRUMENT')
    private Long id

    @NotNull
    @Column(nullable = false)
    String owner

    Long getId() {
        return id
    }
}
