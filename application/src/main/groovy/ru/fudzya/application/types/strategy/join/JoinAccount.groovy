package ru.fudzya.application.types.strategy.join


import javax.persistence.Entity
import javax.persistence.ForeignKey
import javax.persistence.PrimaryKeyJoinColumn

@Entity
@PrimaryKeyJoinColumn(foreignKey = @ForeignKey(name = 'fk_account_id'))
class JoinAccount extends JoinPaymentInstrument {
}
