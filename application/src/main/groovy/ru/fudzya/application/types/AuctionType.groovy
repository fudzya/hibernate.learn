package ru.fudzya.application.types

/**
 * @author fudzya
 * @since  21.09.2018
 */
enum AuctionType {

	HIGHEST_BID,
	LOWEST_BID,
	FIXED_PRICE
}