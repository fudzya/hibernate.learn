package ru.fudzya.application.types

import javax.persistence.*
import javax.validation.constraints.NotNull

@Entity
class Item {

	@Id
	@GeneratedValue(generator = 'ITEM_GENERATOR')
	private Long id

	@NotNull
	@Column(nullable = false)
	private BigDecimal initialPrice

	@Temporal(TemporalType.TIMESTAMP)
	Calendar auctionEnd

	@Column(nullable = false)
	private String name

	@Enumerated(EnumType.STRING)
	private AuctionType auctionType = AuctionType.HIGHEST_BID

	@Transient
	private final Set<Bid> bids = new HashSet<>()

	Long getId() {
		return id
	}

	String getName() {
		return name
	}

	BigDecimal getInitialPrice() {
		return initialPrice
	}

	Set<Bid> getBids() {
		return bids.asImmutable()
	}

	Calendar getAuctionEnd() {
		return auctionEnd
	}

	AuctionType getAuctionType() {
		return auctionType
	}

	void setName(String name) {
		this.name = name
	}

	void setAuctionEnd(Calendar auctionEnd) {
		this.auctionEnd = auctionEnd
	}

	void setAuctionType(AuctionType auctionType) {
		this.auctionType = auctionType
	}

	Item addBid(Bid bid) {
		if (Objects.nonNull(bid.id)) {
			throw new IllegalStateException('Для сохраненных объектов смена зависимостей невозможна')
		}

		bid.setItem(this)
		this.bids.add(bid)

		return this
	}

	Item addBids(Collection<Bid> bids = Collections.emptySet()) {
		
		if (!bids) {
			return
		}

		if (Objects.nonNull(bids.find { bid -> bid.item != null })) {
			throw new IllegalStateException('Для сохраненных объектов смена зависимостей невозможна')
		}

		bids*.setItem(this)
		this.bids.addAll(bids)

		return this
	}
}
