@GenericGenerators([
	@GenericGenerator(
		name       = 'JOIN_PAYMENT_INSTRUMENT',
		strategy   = 'enhanced-sequence',
		parameters = [
			@Parameter(name = 'sequence_name', value = 'S_JOIN_PAYMENT_INSTRUMENT')
		]
	)
])

package ru.fudzya.application.types.strategy.join

import org.hibernate.annotations.GenericGenerator
import org.hibernate.annotations.GenericGenerators
import org.hibernate.annotations.Parameter