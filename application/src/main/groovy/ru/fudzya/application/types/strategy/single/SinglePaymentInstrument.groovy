package ru.fudzya.application.types.strategy.single

import javax.persistence.*
import javax.validation.constraints.NotNull

@Entity
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = 'type')
abstract class SinglePaymentInstrument {

    @Id
    @GeneratedValue(generator = 'SINGLE_PAYMENT_INSTRUMENT')
    private Long id

    @NotNull
    @Column(nullable = false)
    String owner

    Long getId() {
        return id
    }
}
