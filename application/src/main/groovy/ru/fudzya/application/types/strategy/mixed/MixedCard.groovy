package ru.fudzya.application.types.strategy.mixed

import ru.fudzya.application.types.strategy.single.SinglePaymentInstrument

import javax.persistence.Column
import javax.persistence.DiscriminatorValue
import javax.persistence.Entity
import javax.persistence.ForeignKey
import javax.persistence.SecondaryTable
import javax.validation.constraints.NotNull

@Entity
@DiscriminatorValue(value = 'MC')
@SecondaryTable(name = 'mixedcard', foreignKey = @ForeignKey(name = 'pk_card_id'))
class MixedCard extends SinglePaymentInstrument {

    @NotNull
    @Column(table = 'mixedcard', nullable = false)
    private String cardNumber
}
