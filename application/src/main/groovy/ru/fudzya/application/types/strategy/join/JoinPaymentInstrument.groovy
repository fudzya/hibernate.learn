package ru.fudzya.application.types.strategy.join

import javax.persistence.*
import javax.validation.constraints.NotNull

@Entity
@Inheritance(strategy = InheritanceType.JOINED)
/*
 * @DiscriminatorColumn(name = 'type') при её использовании выборка по JoinPaymentInstrument будет использовать
 * не case для определения типа наследника, а дискриминатор
 */
abstract class JoinPaymentInstrument {

    @Id
    @GeneratedValue(generator = 'JOIN_PAYMENT_INSTRUMENT')
    private Long id

    @NotNull
    @Column(nullable = false)
    String owner

    Long getId() {
        return id
    }
}
