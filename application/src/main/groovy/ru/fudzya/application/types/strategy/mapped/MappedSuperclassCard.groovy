package ru.fudzya.application.types.strategy.mapped

import javax.persistence.*
import javax.validation.constraints.NotNull

@Entity
@AttributeOverride(name='owner', column = @Column(name = 'holder', nullable = false))
class MappedSuperclassCard extends MappedSuperclassPaymentInstrument {

    @Id
    @GeneratedValue(generator = "MAPPED_ACCOUNT_GENERATOR")
    private Long id

    @NotNull
    private String number

    @Override
    Long getId() {
        return id
    }

    String getNumber() {
        return number
    }

    void setNumber(String number) {
        this.number = number
    }
}
