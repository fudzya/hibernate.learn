package ru.fudzya.application.types.strategy.single

import javax.persistence.Entity
import javax.validation.constraints.NotNull

@Entity
class SingleCard extends SinglePaymentInstrument {

    @NotNull
    private String number

    String getNumber() {
        return number
    }

    void setNumber(String number) {
        this.number = number
    }
}
