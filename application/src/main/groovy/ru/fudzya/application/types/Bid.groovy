package ru.fudzya.application.types

import org.hibernate.annotations.CreationTimestamp
import org.hibernate.annotations.Immutable

import javax.persistence.*

@Entity
@Immutable
class Bid {

	@Id
	@GeneratedValue(generator = 'BID_GENERATOR')
	private Long id

	@ManyToOne(optional = false, fetch = FetchType.LAZY)
	@JoinColumn(name = 'itemId')
	private Item item

	@Temporal(TemporalType.TIMESTAMP)
	@CreationTimestamp
	private Calendar created

	private BigDecimal amount

	Long getId() {
		return id
	}

	void setItem(Item item) {

		if (this.item == null) {
			this.item = item
			return
		}

		throw new IllegalStateException('Для сохраненных объектов смена зависимостей невозможна')
	}

	Item getItem() {
		return item
	}

	Calendar getCreated() {
		return created
	}

	BigDecimal getAmount() {
		return amount
	}
}
