package ru.fudzya.application.types

/**
 * @author fudzya
 * @since 18.09.2018
 */
class OpValid {

	private Long    id
	private boolean valid

	Long getId() {
		return id
	}

	boolean isValid() {
		return valid
	}

	void setValid(boolean valid) {
		this.valid = valid
	}
}
