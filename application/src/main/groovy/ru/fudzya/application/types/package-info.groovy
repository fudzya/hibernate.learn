@GenericGenerators([
	@GenericGenerator(
		name       = 'USER_GENERATOR',
		strategy   = 'enhanced-sequence',
		parameters = [
			@Parameter(name = 'sequence_name', value = 'S_USER')
	]),

	@GenericGenerator(
		name       = 'BID_GENERATOR',
		strategy   = 'enhanced-sequence',
		parameters = [
			@Parameter(name = 'sequence_name', value = 'S_BID')
	]),

	@GenericGenerator(
		name       = 'ITEM_GENERATOR',
		strategy   = 'enhanced-sequence',
		parameters = [
			@Parameter(name = 'sequence_name', value = 'S_ITEM')
	])
])

@NamedQueries([
	@NamedQuery(name  = 'Hibernate.ItemBids.byId',
				query = 'select ib from ItemBids ib where ib.itemId = :itemId'),

	@NamedQuery(name  = 'Hibernate.ItemBidsWithFormula.byId',
				query = 'select ib from ItemBidsWithFormula ib where ib.itemId = :itemId')
])
package ru.fudzya.application.types

import org.hibernate.annotations.GenericGenerator
import org.hibernate.annotations.GenericGenerators
import org.hibernate.annotations.NamedQueries
import org.hibernate.annotations.NamedQuery
import org.hibernate.annotations.Parameter