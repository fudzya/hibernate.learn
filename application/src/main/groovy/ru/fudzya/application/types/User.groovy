package ru.fudzya.application.types

import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.Id
import javax.validation.Valid
import javax.validation.constraints.NotNull

@Entity
class User implements Serializable {

	@Id
	@GeneratedValue(generator = 'USER_GENERATOR')
	private Long id

	@NotNull
	@Column(nullable = false)
	String firstName

	@NotNull
	@Column(nullable = false)
	String lastName

	@NotNull
	@Column(nullable = false)
	String userName

	@Valid
	Address homeAddress

	Long getId() {
		return id
	}
}
