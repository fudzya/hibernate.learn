package ru.fudzya.application.types

import javax.persistence.Column
import javax.persistence.Embeddable
import javax.validation.constraints.NotNull

/**
 * @author fudzya
 * @since  06.11.2018
 */
@Embeddable
class City implements Serializable {

	@NotNull
	@Column(nullable = false)
	private String name

	@NotNull
	@Column(nullable = false)
	private String zipCode

	@NotNull
	@Column(nullable = false)
	private String country

	String getName() {
		return name
	}

	String getZipCode() {
		return zipCode
	}

	String getCountry() {
		return country
	}

	void setName(String name) {
		this.name = name
	}

	void setZipCode(String zipCode) {
		this.zipCode = zipCode
	}

	void setCountry(String country) {
		this.country = country
	}
}
