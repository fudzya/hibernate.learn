package ru.fudzya.application.types.ext

import org.hibernate.annotations.Formula
import org.hibernate.annotations.Subselect
import org.hibernate.annotations.Synchronize

import javax.persistence.Entity
import javax.persistence.Id

/**
 * @author fudzya
 * @since 19.09.2018
 */
@Entity
@Subselect("""
			select i.id as itemId, i.name as name 
			from Item i 
			left outer join Bid b on i.id = b.itemId
		   """)
@Synchronize(['Bid', 'Item'])
class ItemBidsWithFormula {

	@Id
	private Long   itemId
	private String name

	@Formula('count(itemId)')
	private Long bids

	Long getItemId() {
		return itemId
	}

	String getName() {
		return name
	}

	Long getBids() {
		return bids
	}
}
