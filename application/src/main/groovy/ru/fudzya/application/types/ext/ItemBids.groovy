package ru.fudzya.application.types.ext

import org.hibernate.annotations.Immutable
import org.hibernate.annotations.Subselect
import org.hibernate.annotations.Synchronize

import javax.persistence.Entity
import javax.persistence.Id

@Entity
@Immutable
@Subselect(
		value = """
					select i.id as itemId, i.name as name, count(b.id) as bids from Item i 
					left outer join Bid b on i.id = b.itemId 
					group by i.id, i.name 
				""")
@Synchronize(['Item', 'Bid'])
class ItemBids {

	@Id
	private Long   itemId
	private Long   bids
	private String name

	Long getItemId() {
		return itemId
	}

	String getName() {
		return name
	}

	Long getBids() {
		return bids
	}
}
