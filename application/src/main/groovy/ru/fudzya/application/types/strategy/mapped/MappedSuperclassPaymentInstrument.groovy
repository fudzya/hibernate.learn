package ru.fudzya.application.types.strategy.mapped

import javax.persistence.Column
import javax.persistence.MappedSuperclass
import javax.validation.constraints.NotNull

@MappedSuperclass
abstract class MappedSuperclassPaymentInstrument {

    @NotNull
    @Column(nullable = false)
    String owner

    abstract Long getId()
}
