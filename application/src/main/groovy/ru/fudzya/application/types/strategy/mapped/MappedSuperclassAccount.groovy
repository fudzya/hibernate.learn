package ru.fudzya.application.types.strategy.mapped

import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.Id

@Entity
class MappedSuperclassAccount extends MappedSuperclassPaymentInstrument {

    @Id
    @GeneratedValue(generator = "MAPPED_ACCOUNT_GENERATOR")
    private Long id

    @Override
    Long getId() {
        return id
    }
}
