package ru.fudzya.application.types.strategy.union


import javax.persistence.Entity
import javax.validation.constraints.NotNull

@Entity
class UnionCard extends UnionPaymentInstrument {

    @NotNull
    private String number

    String getNumber() {
        return number
    }

    void setNumber(String number) {
        this.number = number
    }
}
