package ru.fudzya.application.types

import javax.persistence.AttributeOverride
import javax.persistence.AttributeOverrides
import javax.persistence.Column
import javax.persistence.Embeddable
import javax.validation.Valid
import javax.validation.constraints.NotNull

/**
 * @author fudzya
 * @since  21.09.2018
 */
@Embeddable
class Address implements Serializable {

	@NotNull
	@Column(nullable = false)
	private String street

	@Valid
	@NotNull
	@AttributeOverrides([
		@AttributeOverride(name = 'name', column = @Column(name = 'city', nullable = false))
	])
	private City city

	City getCity() {
		return city
	}

	String getStreet() {
		return street
	}

	void setCity(City city) {
		this.city = city
	}

	void setStreet(String street) {
		this.street = street
	}
}
