package ru.fudzya.application.types.strategy.join

import javax.persistence.Entity
import javax.persistence.ForeignKey
import javax.persistence.PrimaryKeyJoinColumn
import javax.validation.constraints.NotNull

@Entity
@PrimaryKeyJoinColumn(foreignKey = @ForeignKey(name = 'fk_card_id'))
class JoinCard extends JoinPaymentInstrument {

    @NotNull
    private String number

    String getNumber() {
        return number
    }

    void setNumber(String number) {
        this.number = number
    }
}
