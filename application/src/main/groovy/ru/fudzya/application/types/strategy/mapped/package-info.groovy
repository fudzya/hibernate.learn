@GenericGenerators([
	@GenericGenerator(
		name       = 'MAPPED_ACCOUNT_GENERATOR',
		strategy   = 'enhanced-sequence',
		parameters = [
			@Parameter(name = 'sequence_name', value = 'S_MAPPED_ACCOUNT')
	]),

	@GenericGenerator(
		name       = 'MAPPED_CARD_GENERATOR',
		strategy   = 'enhanced-sequence',
		parameters = [
			@Parameter(name = 'sequence_name', value = 'S_MAPPED_CARD')
	])
])

package ru.fudzya.application.types.strategy.mapped

import org.hibernate.annotations.GenericGenerator
import org.hibernate.annotations.GenericGenerators
import org.hibernate.annotations.Parameter