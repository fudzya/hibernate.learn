@GenericGenerators([
	@GenericGenerator(
		name       = 'UNION_PAYMENT_INSTRUMENT',
		strategy   = 'enhanced-sequence',
		parameters = [
			@Parameter(name = 'sequence_name', value = 'S_UNION_PAYMENT_INSTRUMENT')
	])
])

package ru.fudzya.application.types.strategy.union

import org.hibernate.annotations.GenericGenerator
import org.hibernate.annotations.GenericGenerators
import org.hibernate.annotations.Parameter