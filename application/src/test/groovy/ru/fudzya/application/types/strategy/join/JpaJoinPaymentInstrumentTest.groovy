package ru.fudzya.application.types.strategy.join

import org.junit.After
import org.junit.Test
import ru.fudzya.db.Type
import ru.fudzya.hibernate.DataAccessServicesRegistry
import ru.fudzya.junit.jpa.JpaSupported

import javax.persistence.EntityManager

class JpaJoinPaymentInstrumentTest extends JpaSupported {

    @Test
    void '[JPA] JoinInstruments. Load all payment instruments'() {
        DataAccessServicesRegistry.INSTANCE.forInstance(Type.JPA).voidCall { EntityManager manager ->

            def query = manager.getCriteriaBuilder().with { builder ->
                return builder.createQuery(JoinPaymentInstrument).with { query ->
                    query.select(query.from(JoinPaymentInstrument))
                    return query
                }
            }

            manager.createQuery(query).getResultList()
        }
    }

    @Override
    protected String jndiDataSourcesName() {
        return 'inheritance'
    }

    @After
    void afterMethod() {
        DataAccessServicesRegistry.INSTANCE.close()
    }
}
