package ru.fudzya.application.types.strategy.join

import org.hibernate.Session
import org.junit.After
import org.junit.Test
import ru.fudzya.db.Type
import ru.fudzya.hibernate.DataAccessServicesRegistry
import ru.fudzya.junit.hibernate.HibernateSupported

class HibernateJoinPaymentInstrumentTest extends HibernateSupported {

    @Test
    void '[Hibernate] JoinInstruments. Load all payment instruments'() {
        DataAccessServicesRegistry.INSTANCE.forInstance(Type.HIBERNATE).voidCall { Session session ->

            def query = session.getCriteriaBuilder().with { builder ->
                return builder.createQuery().with { query ->
                    query.select(query.from(JoinAccount))
                    return query
                }
            }

            session.createQuery(query).list()
        }
    }

    @Override
    protected String jndiDataSourceName() {
        return 'dataSource/join'
    }

    @Override
    protected Collection<String> hibernateConfigFilesName() {
        return ['strategy/join/hibernate.cfg.xml']
    }

    @After
    void afterMethod() {
        DataAccessServicesRegistry.INSTANCE.close()
    }
}
