package ru.fudzya.application.types.strategy.mixed

import org.hibernate.Session
import org.junit.After
import org.junit.Test
import ru.fudzya.application.types.strategy.single.SinglePaymentInstrument
import ru.fudzya.db.Type
import ru.fudzya.hibernate.DataAccessServicesRegistry
import ru.fudzya.junit.hibernate.HibernateSupported

class HibernateMixedPaymentInstrumentTest extends HibernateSupported {

    @Test
    void '[Hibernate] MixedInstruments. Load all payment instruments'() {
        DataAccessServicesRegistry.INSTANCE.forInstance(Type.HIBERNATE).voidCall { Session session ->

            def query = session.getCriteriaBuilder().with { builder ->
                return builder.createQuery().with { query ->
                    query.select(query.from(SinglePaymentInstrument))
                    return query
                }
            }

            session.createQuery(query).list()
        }
    }

    @Override
    protected String jndiDataSourceName() {
        return 'dataSource/mixed'
    }

    @Override
    protected Collection<String> hibernateConfigFilesName() {
        return ['strategy/mixed/hibernate.cfg.xml']
    }

    @After
    void afterMethod() {
        DataAccessServicesRegistry.INSTANCE.close()
    }
}
