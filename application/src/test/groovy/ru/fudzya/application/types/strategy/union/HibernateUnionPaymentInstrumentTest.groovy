package ru.fudzya.application.types.strategy.union

import org.hibernate.Session
import org.junit.After
import org.junit.Test
import ru.fudzya.db.Type
import ru.fudzya.hibernate.DataAccessServicesRegistry
import ru.fudzya.junit.hibernate.HibernateSupported

class HibernateUnionPaymentInstrumentTest extends HibernateSupported {

    @Test
    void '[Hibernate] UnionInstruments. Load all payment instruments'() {
        DataAccessServicesRegistry.INSTANCE.forInstance(Type.HIBERNATE).voidCall { Session session ->

            def query = session.getCriteriaBuilder().with { builder ->
                return builder.createQuery().with { query ->
                    query.select(query.from(UnionPaymentInstrument))
                    return query
                }
            }

            session.createQuery(query).list()
        }
    }

    @Override
    protected String jndiDataSourceName() {
        return 'dataSource/union'
    }

    @Override
    protected Collection<String> hibernateConfigFilesName() {
        return ['strategy/union/hibernate.cfg.xml']
    }

    @After
    void afterMethod() {
        DataAccessServicesRegistry.INSTANCE.close()
    }
}
