package ru.fudzya.application.test.types.ext

import org.hibernate.Session
import org.junit.After
import org.junit.Assert
import org.junit.Test
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import ru.fudzya.application.types.Bid
import ru.fudzya.application.types.Item
import ru.fudzya.application.types.ext.ItemBids
import ru.fudzya.db.Type
import ru.fudzya.hibernate.DataAccessServicesRegistry
import ru.fudzya.junit.hibernate.HibernateSupported

class ItemBidsTest extends HibernateSupported {

	static final Logger LOGGER = LoggerFactory.getLogger(ItemBidsTest)

	@Override
	protected String jndiDataSourceName() {
		return 'itemBids'
	}

	@Override
	protected Collection<String> hibernateConfigFilesName() {
		return ['itemBids/hibernate.cfg.xml']
	}

	@Test
	void '[Hibernate Single Thread] item bids entity load'() {
		DataAccessServicesRegistry.INSTANCE.forInstance(Type.HIBERNATE).voidCall { Session session ->

			def item = new Item(name: 'Horse', initialPrice: 10)
			session.save(item)

			def bids = [
				new Bid(amount: 15),
				new Bid(amount: 20)
			]

			bids*.setItem(item)
			bids.each { bid ->
				session.save(bid)
			}

			item.setName('New Horse')

			LOGGER.info('******** Get ItemBids with find ********')
			ItemBids byFind = session.find(ItemBids, 1L)
			Assert.assertNull(byFind)
			LOGGER.info('****************************************')

			LOGGER.info('******* Get ItemBids with select *******')
			ItemBids bySel = session.getNamedQuery('Hibernate.ItemBids.byId').setParameter('itemId', 1L).getSingleResult() as ItemBids
			Assert.assertNotNull(bySel)
			LOGGER.info('****************************************')
		}
	}

	@After
	void afterMethod() {
		DataAccessServicesRegistry.INSTANCE.close()
	}
}
