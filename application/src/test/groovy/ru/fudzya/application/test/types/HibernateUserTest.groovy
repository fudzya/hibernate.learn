package ru.fudzya.application.test.types

import org.hibernate.Session
import org.junit.After
import org.junit.Test
import ru.fudzya.application.types.User
import ru.fudzya.db.Type
import ru.fudzya.hibernate.DataAccessServicesRegistry
import ru.fudzya.junit.hibernate.HibernateSupported

import javax.validation.ConstraintViolationException

/**
 * @author sbt-balovtsev-in
 * @since 06.11.2018
 */
class HibernateUserTest extends HibernateSupported {

	@Test(expected = ConstraintViolationException.class)
	void 'null address test'() {
		DataAccessServicesRegistry.INSTANCE.forInstance(Type.HIBERNATE).voidCall { Session session ->
			User user = new User(firstName: '', lastName: '', userName: '', homeAddress: [:])
			session.save(user)
		}
	}

	@After
	void after() throws Exception {
		DataAccessServicesRegistry.INSTANCE.close()
	}

	@Override
	protected String jndiDataSourceName() {
		return 'user'
	}

	@Override
	protected Collection<String> hibernateConfigFilesName() {
		return ['/user/hibernate.cfg.xml']
	}
}
