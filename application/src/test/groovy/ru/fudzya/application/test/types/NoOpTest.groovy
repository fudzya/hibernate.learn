package ru.fudzya.application.test.types

import org.hibernate.Session
import org.junit.After
import org.junit.Assert
import org.junit.Test
import ru.fudzya.application.types.NoOpValid
import ru.fudzya.application.types.OpValid
import ru.fudzya.db.Type
import ru.fudzya.hibernate.DataAccessServicesRegistry
import ru.fudzya.junit.hibernate.HibernateSupported

import javax.persistence.criteria.CriteriaQuery
import javax.persistence.criteria.Root

/**
 * @author fudzya
 * @since 18.09.2018
 */
class NoOpTest extends HibernateSupported {

	@Override
	protected String jndiDataSourceName() {
		return 'noop'
	}

	@Override
	protected Collection<String> hibernateConfigFilesName() {
		return ['/noop/hibernate.cfg.xml']
	}

	@Test
	void '[Hibernate Single Thread] Entity noop property is null'() {
		DataAccessServicesRegistry.INSTANCE.forInstance(Type.HIBERNATE).voidCall { Session session ->

			def noopId = session.save(new NoOpValid())

			CriteriaQuery<NoOpValid> query = session.getCriteriaBuilder().with { builder ->
				return builder.createQuery(NoOpValid).with { query ->

					Root<NoOpValid> root = query.from(NoOpValid)
					query.where(builder.and(builder.equal(root.get('id'), builder.parameter(Long, 'id')),
						                    builder.isNull(root.get('valid'))))
				}
			}

			Assert.assertNotNull(session.createQuery(query).setParameter('id', noopId).uniqueResult())
		}
	}

	@Test
	void '[Hibernate Single Thread] Entity noop property not null'() {
		DataAccessServicesRegistry.INSTANCE.forInstance(Type.HIBERNATE).voidCall { Session session ->

			def noopId = session.save(new OpValid(valid: true))

			CriteriaQuery<NoOpValid> query = session.getCriteriaBuilder().with { builder ->
				return builder.createQuery(NoOpValid).with { query ->

					Root<NoOpValid> root = query.from(NoOpValid)
					query.where(builder.and(builder.equal(root.get('id'), builder.parameter(Long, 'id')),
								builder.isTrue(root.get('valid'))))
				}
			}

			Assert.assertNotNull(session.createQuery(query).setParameter('id', noopId).uniqueResult())
		}
	}

	@After
	void after() throws Exception {
		DataAccessServicesRegistry.INSTANCE.close()
	}
}
